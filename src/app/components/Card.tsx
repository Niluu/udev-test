export const CARD = [
  {
    title: "New",
    id: 0,
    bgColor: "bg-blue-500",
  },
  {
    title: "Zagatovka",
    id: 1,
    bgColor: "bg-amber-300",
  },
  {
    title: "Done",
    id: 2,
    bgColor: "bg-green-500",
  },
  {
    title: "In a way",
    id: 4,
    bgColor: "bg-emerald-500",
  },
];

interface CardProps {
  id: number;
  title: string;
  children: any;
  bgColor: string;
}

export default function Card(props: CardProps) {
  const { id, title, children, bgColor } = props;

  return (
    <div className="min-w-[360px] w-full py-6">
      <h1 className={`${bgColor} p-4 text-white rounded-t-md  font-bold`}>
        {title} ({id})
      </h1>
      <div className="bg-slate-100 h-fit py-4">{children}</div>
    </div>
  );
}

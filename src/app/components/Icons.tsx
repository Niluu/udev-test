import { BiSolidBarChartSquare } from "react-icons/bi";
import {
  AiOutlineSearch,
  AiOutlineClockCircle,
  AiFillCar,
  AiOutlineInfoCircle,
  AiFillClockCircle,
  AiOutlineCheck,
  AiOutlineClose,
  AiFillSetting,
} from "react-icons/ai";
import { BsCardChecklist, BsChevronDown } from "react-icons/bs";
import { MdPayment } from "react-icons/md";

/**
 * Icon Component
 *
 * The Icon componets renders icons from react-icons
 *
 * Learn More:
 * - [Ract icons](https://react-icons.github.io/react-icons)
 *
 */

export const Icons = {
  BiSolidBarChartSquare,
  AiOutlineSearch,
  AiOutlineClockCircle,
  AiFillCar,
  AiOutlineInfoCircle,
  AiFillClockCircle,
  AiOutlineCheck,
  AiOutlineClose,
  BsCardChecklist,
  BsChevronDown,
  MdPayment,
  AiFillSetting,
};

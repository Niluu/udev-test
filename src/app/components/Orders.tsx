import { Icons } from "./Icons";

interface Order {
  id: string;
  name: string;
  quantity: number;
  option?: string[];
  totalCost: number;
  orderdTime: string;
  comments?: string[];
  status?: "primary" | "secondary" | "defoult";
}

interface OrdersProps {
  order: Order;
}

export default function Orders({ order }: OrdersProps) {
  const {
    id,
    name,
    quantity,
    option,
    totalCost,
    orderdTime,
    comments,
    status,
  } = order;

  return (
    <div className="bg-white rounded-md m-4 cursor-pointer">
      <div className="flex justify-between items-center border-b">
        <div className="flex items-center p-2 m-2">
          <h1 className="font-bold">ID: {id}</h1>
          <Icons.AiOutlineInfoCircle className="w-6 h-4 text-slate-500 " />
        </div>
        <div className="w-30 flex justify-around items-center">
          <p className="text-sm text-slate-500">{totalCost} sum</p>
          <Icons.MdPayment className="w-6 h-4 text-slate-500" />
          <Icons.AiFillCar className="w-6 h-4  text-slate-500" />
        </div>
      </div>

      {/* Card order */}
      <div className="p-2 m-2 ">
        <h1 className="font-bold">
          {quantity} <span>X</span>
          {name}
        </h1>
        {option
          ? option.map((optionItem) => (
              <p
                key={optionItem + id}
                className="relative left-6 text-sm text-gray-400"
              >
                {optionItem}
              </p>
            ))
          : null}
      </div>

      {/* Card time */}
      <div className="flex justify-end items-center p-2 m-2">
        <Icons.AiFillClockCircle className="w-6 h-4  text-slate-500 " />
        <p className="text-slate-500 text-sm">{orderdTime}</p>
      </div>

      {/* Cart option */}
      <div className={`${status && "border-t  border-slate-300"} `}>

        {comments ? (
          <div className="flex justify-between items-center p-2 cursor-pointer">
            <h1 className="text-slate-500 ">
              Comments ({comments.length})
              <span className="bg-blue-500 px-2 py-0 rounded-full text-white">
                + {comments.length}
              </span>
            </h1>
            <Icons.BsChevronDown className="w-6 h-4 text-blue-500" />
          </div>
        ) : null}

        {/* Option 1 */}
        {status == "defoult" ? (
          <div className="flex justify-between p-1">
            <button className="w-full border-solid border border-slate-300 rounded-md flex justify-center items-center p-1.5 m-0.5">
              <Icons.AiOutlineClose className="w-6 h-4 text-red-500" />
              <p className="text-red-500">Otmenit</p>
            </button>
            <button className="w-full bg-blue-500 rounded-md flex justify-center items-center p-1.5 m-0.5">
              <Icons.AiOutlineCheck className="w-6 h-4 text-white" />
              <p className="text-white">Prinyat</p>
            </button>
          </div>
        ) : null}

        {/* Option 2 */}
        {status == "primary" ? (
          <div className="flex justify-between p-1">
            <button className="w-full border-solid border border-blue-500 rounded-md flex justify-center items-center p-1.5 m-0.5">
              <Icons.AiOutlineSearch className="w-6 h-4 text-blue-500" />
              <p className="text-blue-500">Prinyat</p>
            </button>
          </div>
        ) : null}

        {/* Option 3 */}
        {status === "secondary" ? (
          <div className="flex justify-between p-1">
            <button className="w-full border-solid border border-blue-500 rounded-md flex justify-center items-center p-1.5 m-0.5">
              <p className="text-blue-500">Zavershit</p>
            </button>
          </div>
        ) : null}
      </div>
    </div>
  );
}

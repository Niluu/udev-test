"use client";
import { useState } from "react";
import Image from "next/image";
import { Icons } from "./components/Icons";
import Card , { CARD } from "./components/Card";
import orders from "./helper/mock.json";
import Orders from "./components/Orders";

export default function Home() {
  const [search, setSearch] = useState("");

  const handleInputChange = (e: any) => {
    setSearch(e.target.value);
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    console.log("Form submitted with value:", search);
  };

  return (
    <div>
      {/* Header */}
      <header className="w-full flex items-center relative overflow-hidden">
        <div className="w-10 h-10 flex justify-center items-center rounded-full bg-blue-500 text-white m-2 font-bold text-2xl">
          D
        </div>
        <h1 className="font-bold p-2 text-lg">Todays orders</h1>
        <span className="h-px w-screen bg-slate-200 absolute bottom-0 " />
      </header>

      {/* Layout */}
      <div className="flex relative">
        <div className="w-16 min-h-full">
          <div className="w-10 h-10 bg-blue-500 rounded-md flex justify-center items-center m-2">
            <Icons.BiSolidBarChartSquare color="white" />
          </div>
          <div className="absolute bottom-2 overflow-hidden">
            <Icons.AiFillSetting className="w-8 h-6 text-slate-500 m-auto mb-6" />
            <span className="h-px w-screen bg-slate-200 absolute bottom-16 " />
            <div className="w-14 h-14 rounded-full overflow-hidden ">
              <Image
                src="/ava.jpg"
                alt="Profile photo"
                width={50}
                height={50}
              />
            </div>
          </div>
          <div className="w-px min-h-full bg-slate-200 absolute -top-16 bottom-0 left-14" />
        </div>

        <div className="w-full p-2">
          {/* Searchbar */}
          <div className="flex justify-between">
            <div className="flex">
              <form
                onSubmit={handleSubmit}
                className="flex relative items-center"
              >
                <Icons.AiOutlineSearch className="w-8 h-6 absolute left-0.5 text-blue-500 " />
                <input
                  type="text"
                  className="p-1.5 border border-gray-300 rounded-md outline-0 focus:outline focus:outline-1 focus:outline-blue-600 indent-6"
                  placeholder="Search ID"
                  value={search}
                  onChange={handleInputChange}
                />
              </form>
            </div>
            <div className="flex justify-between">
              <div className="flex justify-around items-center border rounded-md p-1.5 mx-4 cursor-pointer">
                <Icons.BsCardChecklist className="w-10 h-6 text-blue-500" />
                <h1>All: 115</h1>
                <Icons.BsChevronDown className="w-10 h-4 text-blue-500" />
              </div>
              <div className="flex justify-around items-center border rounded-md p-1.5">
                <Icons.AiOutlineClockCircle className="w-10 h-6 text-blue-500" />
                <h1 className="font-bold">45:00</h1>
              </div>
            </div>
          </div>

          {/* Card layout */}
          <div className="grid grid-cols-1 sm:grid-cols-4 gap-x-6 xxl:grid-cols-4 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2">
            {CARD.map((card) => (
              <Card
                key={card.title}
                id={card.id}
                title={card.title}
                bgColor={card.bgColor}
              >
                {orders[card.id] ? (
                  orders[card.id].map((order: any) => (
                    <Orders key={order.id} order={order} />
                  ))
                ) : (
                  <p className="p-4">No orders for this card {card.id}</p>
                )}
              </Card>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
